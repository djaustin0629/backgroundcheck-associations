package com.talentreef.backgroundcheck.associations

import java.io.File

import com.github.tototoshi.csv._
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.collection.mutable.ArrayBuffer

object Main {

  final val PROP_ID: Int = 0
  final val PROP_NUM: Int = 1
  final val PROP_NAME: Int = 2
  final val POSITION_ID: Int = 3
  final val POSITION_NAME: Int = 4
  final val JDP_CLIENT: Int = 5

  import LocPosAssignments._

  def main(args: Array[String]) = {
    println("Associations")

    // TODO:
    //  1. figure out how to obtain clientId: String
    //     - GET      /v1/clients/:clientId/backgroundCheckConfig/bundles
    //  2. build Seq[LocPropAssignments],
    //  3. PUT      /v1/clients/:clientId/backgroundCheckConfig/bundles/:bundleId/assignments
    val reader = CSVReader.open(new File(args(0)))

   // val clientId = args(1)
    val locPosAssignments = ArrayBuffer.empty[LocPosAssignments]
    val iterator = reader.iterator

    var rowNum = 1
    while (iterator.hasNext) {
      iterator.next()
      if (rowNum > 1) {
        val row = iterator.next().toList
        val position = Position(row(POSITION_ID).toInt, row(POSITION_NAME))
        val location = Location(row(PROP_ID).toInt, row(PROP_NUM), row(PROP_NAME), None, None)
        locPosAssignments += LocPosAssignments(position, Seq(location))
      }
      rowNum += 1
    }

    reader.close
    println(s"LOC POS ASSIGNMENTS ${Json.toJson(locPosAssignments)}")
  }

}

case class LocPosAssignments(position: Position, locations: Seq[Location])
object LocPosAssignments {

  implicit val locPosAssignmentsReads: Reads[LocPosAssignments] = (
    (JsPath \ "position").read[Position] and
    (JsPath \ "locations").read[Seq[Location]]
  )(LocPosAssignments.apply _)

  implicit val locPocAssignmentWrites: Writes[LocPosAssignments] = (
    (JsPath \ "position").write[Position] and
      (JsPath \ "locations").write[Seq[Location]]
    )(unlift(LocPosAssignments.unapply))
}

case class Location(propId: Int,
                    propNum: String,
                    propName: String,
                    state: Option[String],
                    brand: Option[Seq[String]])
object Location {
  implicit val locationFormat: Format[Location] = (
    (JsPath \ "propId").format[Int] and
      (JsPath \ "propNum").format[String] and
      (JsPath \ "propName").format[String] and
      (JsPath \ "state").formatNullable[String] and
      (JsPath \ "brand").formatNullable[Seq[String]]
    )(Location.apply, unlift(Location.unapply))
}

case class Position(id: Int, name: String)
object Position {
  implicit val positionFormat: Format[Position] = (
    (JsPath \ "positionId").format[Int] and
      (JsPath \ "positionName").format[String]
    )(Position.apply, unlift(Position.unapply))
}