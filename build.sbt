import sbt._
name := """backgroundcheck-associations"""
organization := "com.talentreef"
scalaVersion := "2.11.12"

libraryDependencies ++=
  Seq(
    "com.typesafe.play" %% "play-json" % "2.5.3",
    "com.typesafe.play" %% "play" % "2.5.3",
    "com.github.tototoshi" %% "scala-csv" % "1.3.6")

mainClass in (Compile, run) :=
  Some("com.talentreef.backgroundcheck.associations.Main")